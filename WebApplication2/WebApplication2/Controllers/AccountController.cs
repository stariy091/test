﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class AccountController : Controller
    {
        [HttpGet]
        public ActionResult Register()
        {
            RegisterViewModel model;
            if (Request.Cookies["model"] != null)
            {
                model = JsonConvert.DeserializeObject<RegisterViewModel>(Request.Cookies["model"].Value);
                model.ListSalutation.RemoveRange(5,6);
            }
            else model = new RegisterViewModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                Response.Cookies.Add(new HttpCookie("model", JsonConvert.SerializeObject(model)));
                return RedirectToAction("RegisterConfirm");
            }
            return View(model);
        }
        public ActionResult RegisterConfirm()
        {
            RegisterViewModel model = JsonConvert.DeserializeObject<RegisterViewModel>(Request.Cookies["model"].Value);
            return View(model);
        }
        public ActionResult AddUser()
        {
            if (Request.Cookies["model"] != null)
            {
                var cookie = new HttpCookie("model")
                {
                    Expires = DateTime.Now.AddDays(-1d)
                };
                Response.Cookies.Add(cookie);
            }
            return RedirectToAction("Login");
        }
    }
}