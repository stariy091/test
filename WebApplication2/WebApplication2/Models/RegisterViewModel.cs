﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebApplication2.Models
{
    public class RegisterViewModel
    {
        [Display(Name = "Salutation")]
        public List<SelectListItem> ListSalutation { get; set; }
        [Required(ErrorMessage = "Field Salutatio necessarily")]
        [Display(Name = "Salutation")]
        public string SelectedSalutation { get; set; }
        [Required(ErrorMessage = "Field First Name necessarily")]
        [Display(Name ="First Name")]
        public string FirstName { get; set; }
        [Display(Name ="Middle Name")]
        public string MiddleName { get; set; }
        [Required(ErrorMessage = "Field Last Name necessarily")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Field Company necessarily")]
        public string Company { get; set; }
        [Required(ErrorMessage = "Field Title necessarily")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Field Email necessarily")]
        [EmailAddress(ErrorMessage ="Field Email incorrect")]
        public string Email { get; set; }
        [System.ComponentModel.DataAnnotations.Compare("Email",ErrorMessage = "Fields Email and Email Confirm different")]
        [Display(Name = "Confirm Email")]
        
        public string ConfirmEmail { get; set; }
        [Required(ErrorMessage = "Field Phone necessarily")]
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }
        
        public RegisterViewModel()
        {
            ListSalutation = new List<SelectListItem>() {
                new SelectListItem(),
                new SelectListItem() {Text="-Uncpecified-",Value="Uncpecified" },
                new SelectListItem() {Text="Mr.",Value="Mr."},
                new SelectListItem() {Text="Mrs.",Value="Mrs." },
                new SelectListItem() {Text="Ms",Value="Ms"},
                new SelectListItem() {Text="Dr.",Value="Dr."}
            };
        }
    }
}